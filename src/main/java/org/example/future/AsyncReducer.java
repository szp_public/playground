package org.example.future;

import org.example.function.DelayedMultiplyBy2;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class AsyncReducer {


  public static void main(String[] args) throws Throwable {
    List<Integer> inputs = Arrays.asList(1, 2, 3, 4, 5);

    DelayedMultiplyBy2 multiplyBy2 = new DelayedMultiplyBy2();
    final List<CompletableFuture<Integer>> futures = inputs.stream()
      .map(i->multiplyBy2.apply(i))
      .collect(Collectors.toList());

    //Chaining
    futures.add(multiplyBy2.apply(10).thenApply(r -> r * 10).thenApply(d -> d / 4));

    //Exception handling example. When error ocurrs, return 0
    futures.add(CompletableFuture.supplyAsync(() -> {
      if (true)
        throw new RuntimeException();
      return Integer.MAX_VALUE;
    }).exceptionally(e -> 0));

    final List<Integer> computedFutures = futures.stream().map(CompletableFuture::join).collect(Collectors.toList());
    final Optional<Integer> sum = computedFutures.stream().reduce((op1, op2) -> op1 + op2);
    System.out.println("sum = " + sum.get());


  }
}
