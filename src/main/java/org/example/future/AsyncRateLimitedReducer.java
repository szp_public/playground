package org.example.future;

import org.example.function.DelayedMultiplyBy2;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class AsyncRateLimitedReducer {

  public static final int RATE_LIMIT = 10;
  public static final int MAX = 50;
  public static final int WAIT_MILLIS = 1000;

  public static void main(String[] args) {

    final long before = System.currentTimeMillis();

    ExecutorService executor = Executors.newFixedThreadPool(RATE_LIMIT);
    DelayedMultiplyBy2 multiplyBy2 = new DelayedMultiplyBy2(executor);
    final List<Integer> inputs = IntStream.range(0, MAX).boxed().collect(Collectors.toList());

    final List<CompletableFuture<Integer>> futures = inputs.stream()
      .map(i -> multiplyBy2.apply(i))
      .collect(Collectors.toList());
    futures.add(multiplyBy2.apply(100).thenApplyAsync(r->r*10,executor).thenApplyAsync(d->d/4,executor));

    final List<Integer> computedFutures = futures.stream().map(CompletableFuture::join).collect(Collectors.toList());
    final Optional<Integer> sum = computedFutures.stream().reduce((op1, op2) -> op1 + op2);
    System.out.println("sum = " + sum.get());
    final long now = System.currentTimeMillis();
    System.out.println("took "+(now-before));

    executor.shutdown();


  }



}
