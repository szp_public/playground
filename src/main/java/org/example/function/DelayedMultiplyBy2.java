package org.example.function;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.function.BiFunction;
import java.util.function.Function;


public class DelayedMultiplyBy2 implements Function<Integer,  CompletableFuture<Integer>> {

  static final int WAIT_MILLIS = 1000;

  private Executor executor;

  public DelayedMultiplyBy2() {
  }

  public DelayedMultiplyBy2(Executor executor) {
    this.executor = executor;
  }

  @Override
  public CompletableFuture<Integer> apply(Integer num) {
    if (this.executor == null) {
      return CompletableFuture.supplyAsync(() -> {
        try {
          Thread.sleep(WAIT_MILLIS);
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
        return num * 2;
      });
    }
    return CompletableFuture.supplyAsync(() -> {
      try {
        Thread.sleep(WAIT_MILLIS);
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
      return num * 2;
    }, this.executor);
  }
}
